﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Helios.Service;
using Helios.Service.Contract;

namespace Helios.DistributedService.Controllers
{
    public class ProductsController : ApiController
    {
        private ProductService _productService = new ProductService();

        // GET api/products
        public IEnumerable<ProductDto> Get()
        {
            return new ProductDto[] { new ProductDto() };
        }

        // GET api/products/sku1
        public ProductDto Get(string sku)
        {
            return _productService.GetProductBySku(sku);
        }

        // POST api/products
        public void Post([FromBody]ProductDto productDto)
        {
            _productService.AddProduct(productDto);
            //_productService.AddProduct();
        }

        // PUT api/products/5
        public void Put(int id, [FromBody]ProductDto value)
        {
        }

        // DELETE api/products/5
        public void Delete(int id)
        {
        }
    }
}
