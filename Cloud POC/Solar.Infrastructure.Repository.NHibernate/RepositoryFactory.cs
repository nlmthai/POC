﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System.Diagnostics.Contracts;
using System.Security;
using NHibernate;

[assembly: SecurityRules(SecurityRuleSet.Level1)]
namespace Solar.Infrastructure.Repository.NHibernate
{
    public class RepositoryFactory : IRepositoryFactory
    {
        public RepositoryFactory(ISessionFactory sessionFactory)
        {
            Contract.Requires(sessionFactory != null);

            this.sessionFactory = sessionFactory;
        }

        protected readonly ISessionFactory sessionFactory;

        #region IDisposable Members

        private bool disposed = false;

        public void Dispose()
        {
            if (!this.disposed)
            {
                this.sessionFactory.Dispose();

                this.disposed = true;
            }
        }

        #endregion        

        #region IRepositoryFactory Members

        public IRepository CreateRepository()
        {
            return new Repository(this.sessionFactory.OpenSession());
        }

        #endregion        
    }
}
