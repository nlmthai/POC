﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Transform;

namespace Solar.Infrastructure.Repository.NHibernate
{
    public class Repository : IRepository
    {
        static Repository()
        {
            //HibernatingRhinos.NHibernate.Profiler.Appender.NHibernateProfiler.Initialize();
        }

        public Repository(ISession session)
        {
            Contract.Requires(session != null && session.IsConnected && session.IsOpen);

            this.session = session;
        }

        protected readonly ISession session;

        #region IDisposable Members

        private bool disposed = false;

        public void Dispose()
        {
            if (!this.disposed)
            {
                this.session.Clear();

                this.session.Dispose();

                this.disposed = true;
            }
        }

        #endregion

        #region IRepository Members

        public TEntity Get<TEntity>(object id) where TEntity : class
        {
            return this.session.Get<TEntity>(id);
        }

        public IList<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class
        {
            return this.session.Query<TEntity>().Where(filter).ToList();
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class
        {
            return this.session.Query<TEntity>();
        }

        public object Add(object entity)
        {
            using (var transaction = this.session.BeginTransaction())
            {
                var id = this.session.Save(entity);

                transaction.Commit();

                return id;
            }
        }

        public void Update(object entity)
        {
            using (var transaction = this.session.BeginTransaction())
            {
                if (this.session.Contains(entity))
                {
                    entity = this.session.Merge(entity);
                }

                this.session.Update(entity);

                transaction.Commit();
            }
        }

        public void Remove(object entity)
        {
            using (var transaction = this.session.BeginTransaction())
            {
                this.session.Delete(entity);

                transaction.Commit();
            }
        }

        public void Refresh(object entity)
        {
            this.session.Refresh(entity);
        }

        public void Stale(object entity)
        {
            this.session.Evict(entity);
        }

        public void AddOrUpdate(object entity)
        {
            using (var transaction = this.session.BeginTransaction())
            {
                this.session.SaveOrUpdate(entity);

                transaction.Commit();
            }
        }

        public IUnitOfWork CreateUnitOfWork()
        {
            return new UnitOfWork(this.session);
        }

        public IList<TEntity> ExecuteQuery<TEntity>(string tSQL) where TEntity : class
        {
            return this.session
                .CreateSQLQuery(tSQL)
                .SetResultTransformer(new AliasToBeanResultTransformer(typeof(TEntity)))
                .List<TEntity>();

        }
        #endregion
    }
}
