﻿using Solar.Core;
using Solar.Infrastructure.Repository;

namespace Helios.Domain
{
    public class Product : DomainEntity<int>
    {
        [MaxLength(50)]
        public virtual string Sku { get; set; }
        public virtual string Name { get; set; }

        [ReadOnly]
        public virtual string BrowsingName { get; set; }
    }
}
