﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Solar.Core;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace Helios.Domain
{
    public class Category : DomainEntity<int>
    {
        [MaxLength(50)]
        public virtual string Sku { get; set; }
        public virtual string Name { get; set; }
        public virtual Category SubCategory { get; set; }
    }

    public class Item : DomainEntity<int>
    {
        [MaxLength(50)]
        public virtual string Sku { get; set; }
        public virtual string Name { get; set; }
        public virtual List<Item> SubItems { get; set; }
    }

    public class Book : DomainEntity<int>
    {
        [MaxLength(50)]
        public virtual string Sku { get; set; }
        public virtual string Name { get; set; }
        public virtual List<Major> Majors { get; set; }
    }

    public class Major : DomainEntity<int>
    {
        public virtual string Name { get; set; }
        public virtual List<Book> Books { get; set; }
    }

    public class MapBook : EntityTypeConfiguration<Book>
    {
        public MapBook()
        {
            HasMany(a => a.Majors)
            .WithMany(b => b.Books)
            .Map(m => m.MapLeftKey("BookId")
                .MapRightKey("MajorId")
                .ToTable("Test"));
        }
    }
}
