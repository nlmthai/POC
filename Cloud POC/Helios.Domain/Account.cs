﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Solar.Core;

namespace Helios.Domain
{
    public class Account : DomainEntity<int>
    {
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
    }
}
