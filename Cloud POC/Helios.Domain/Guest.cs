﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Solar.Core;
//using System.ComponentModel.DataAnnotations;

namespace Helios.Domain
{
    public class Guest : DomainEntity<int>
    {
        //[StringLength(1)]
        public virtual string Ip { get; set; }
        public virtual string UserAgent { get; set; }
    }
}
