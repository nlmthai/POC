﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;

namespace Solar.Security.Web
{
    public interface IAuthenticationService
    {   
        bool Authenticate(string userId, string password);
        bool Authenticate(string userId, string password, TimeSpan expirationTimeWindow, bool persistsAuthentication);

        void SignOut();
    }
}
