﻿using NCron;

namespace Helios.Service.ScheduledTasks
{
    public class ClearCacheCronJob : CronJob
    {
        public override void Execute()
        {
            Log.Info(() => string.Format("Cache is clear..{0}", GetHashCode()));
        }
    }
}
