﻿using Helios.Service.Contract;
using Solar.Core;
using Helios.Domain;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Web;
using System.Configuration;
using System.IO;
using Solar.Infrastructure;

namespace Helios.Service
{
    public class ProductService
    {
        public void AddProduct(ProductDto product)
        {
            using (var repository = DomainRepository.Open())
            {
                //var _product = new ProductDto
                //{
                //    Sku = "Sku1",
                //    Name = "Product Name"
                //};

                repository.Add(product.ExposedAs<Product>(repository));
            }

            using (var repository2 = DomainRepository.Open())
            {
                using (var unitOfWork = repository2.CreateUnitOfWork())
                {
                    var product2 = unitOfWork.Add(new Product() { Sku = "abc" });

                    unitOfWork.Commit();
                }
            }
        }


        public ProductDto GetProductBySku(string sku)
        {
            using (var repository = DomainRepository.Open())
            {
                return repository.GetOne<Product>(p => p.Sku.Equals(sku))
                    .ExposedAs<ProductDto>();
            }
        }
    }
}

