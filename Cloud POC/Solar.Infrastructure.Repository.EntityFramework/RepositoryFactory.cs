﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using Solar.Infrastructure.Repository.EntityFramework.DAL;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics.Contracts;
using System.Security;

[assembly: SecurityRules(SecurityRuleSet.Level1)]
namespace Solar.Infrastructure.Repository.EntityFramework
{
    public class RepositoryFactory : IRepositoryFactory
    {
        protected readonly string _connection;
        public RepositoryFactory(string connection)
        {
            this._connection = connection;
        }

        private bool disposed = false;
        public void Dispose()
        {
            if (!this.disposed)
            {
                //this._connection.Dispose();
                this.disposed = true;
            }
        }

        public IRepository CreateRepository()
        {
            return new Repository(new DatabaseContext(_connection));
        }
    }
}
