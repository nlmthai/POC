﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;

namespace Solar.Infrastructure.Repository.EntityFramework
{
    public class Repository : IRepository
    {
        private DbContext _context;
        public Repository(DbContext context)
        {
            _context = context;
        }

        #region IDisposable Members

        private bool disposed = false;

        public void Dispose()
        {
            if (!this.disposed)
            {
                this._context.Dispose();

                this.disposed = true;
            }
        }

        #endregion

        #region IRepository Members

        /// <summary>
        /// Get entity by ID
        /// </summary>
        /// <typeparam name="TEntity">Type of entity - table in DB</typeparam>
        /// <param name="id">It cannot is complex type</param>
        /// <returns>Entity</returns>
        public TEntity Get<TEntity>(object id) where TEntity : class
        {
            return this._context.Set<TEntity>().Find(id);
        }

        /// <summary>
        /// Find by condition
        /// </summary>
        /// <typeparam name="TEntity">Table in DB</typeparam>
        /// <param name="filter">condition</param>
        /// <returns>IList of table</returns>
        public IList<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter) 
            where TEntity : class
        {
            return this._context.Set<TEntity>().Where(filter).ToList();
        }

        /// <summary>
        /// Get entities from database
        /// </summary>
        /// <typeparam name="TEntity">Table in DB</typeparam>
        /// <returns>IQueryable of table</returns>
        public IQueryable<TEntity> Query<TEntity>() where TEntity : class
        {
            return this._context.Set<TEntity>();
        }

        /// <summary>
        /// Add entity and call save changes
        /// </summary>
        /// <param name="entity">Added entity</param>
        /// <returns>Added entity from database</returns>
        public object Add(object entity)
        {
            var returnedEntity = this._context.Set(entity.GetType()).Add(entity);
            this._context.SaveChanges();
            return returnedEntity;
        }

        /// <summary>
        /// Update entity and call save changes
        /// </summary>
        /// <param name="entity">updated entity</param>
        public void Update(object entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            this._context.SaveChanges();
        }

        /// <summary>
        /// Remove entity from context and call save changes
        /// </summary>
        /// <param name="entity">deleted entity</param>
        public void Remove(object entity)
        {
            this._context.Set(entity.GetType()).Remove(entity);
            this._context.SaveChanges();
        }

        /// <summary>
        /// Reload entity from db to dbContext
        /// </summary>
        /// <param name="entity">new entity</param>
        public void Refresh(object entity)
        {
            this._context.Entry(entity).Reload();
        }

        /// <summary>
        /// Delete entity in DbContext
        /// </summary>
        /// <param name="entity">deleted entity</param>
        public void Stale(object entity)
        {
            this._context.Set(entity.GetType()).Remove(entity);
        }

        /// <summary>
        /// Add new or Update (if existed)
        /// </summary>
        /// <param name="entity">updated entity</param>
        public void AddOrUpdate(object entity)
        {
            var entityDatabase = this._context.Entry(entity).Entity;
            if (entityDatabase != null) //update
            {
                Update(entity);
            }
            else //create
            {
                Add(entity);
            }
        }

        /// <summary>
        /// Considering ...
        /// </summary>
        /// <returns></returns>
        public IUnitOfWork CreateUnitOfWork()
        {
            return new UnitOfWork(_context);
        }

        /// <summary>
        /// Create sql to execute
        /// </summary>
        /// <typeparam name="TEntity">Executed table on Db</typeparam>
        /// <param name="tSQL">sql</param>
        /// <returns>List of entities</returns>
        public IList<TEntity> ExecuteQuery<TEntity>(string tSQL) where TEntity : class
        {
            return this._context
                .Set<TEntity>().SqlQuery(tSQL).ToList<TEntity>();
        }
        #endregion
    }
}
