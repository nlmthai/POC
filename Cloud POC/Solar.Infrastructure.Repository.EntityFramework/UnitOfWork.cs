﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using Solar.Infrastructure.Repository.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Transactions;

namespace Solar.Infrastructure.Repository.EntityFramework
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        protected internal readonly DbContext _context;
        protected internal bool _disposed;

        TransactionScope transaction;
        public UnitOfWork(DbContext dbContext)
        {
            transaction = new TransactionScope();
            _context = dbContext;
        }

        public void Rollback()
        {
            //if exception is throw, transaction scope call rollback automatically
            _context.ChangeTracker.Entries().ToList().ForEach(x => x.Reload());
        }

        private bool disposed = false;
        public void Dispose()
        {
            if (!this.disposed)
            {
                this.transaction.Dispose();
                this._context.Dispose();
                this.disposed = true;
            }            
            GC.SuppressFinalize(this);
        }
        

        public object Add(object entity)
        {
            var returnedEntity = this._context.Set(entity.GetType()).Add(entity);
            this._context.SaveChanges();
            return returnedEntity;
        }

        public void AddOrUpdate(object entity)
        {
            var entityDatabase = this._context.Entry(entity).Entity;
            if (entityDatabase != null) //update
            {
                Update(entity);
            }
            else //create
            {
                Add(entity);
            }
        }

        public void Commit()
        {
            transaction.Complete();
        }

        public void Remove(object entity)
        {
            this._context.Set(entity.GetType()).Remove(entity);
            _context.SaveChanges();
        }

        public void Update(object entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            this._context.SaveChanges();
        }
    }
}
