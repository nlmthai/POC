﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Solar.Infrastructure.Repository.EntityFramework.DAL
{
    public class CustomConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("assemblies", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(CustomConfigCollection))]
        public CustomConfigCollection Assemblies
        {
            get
            {
                return (CustomConfigCollection)base["assemblies"];
            }
        }
    }

    public class CustomConfigCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CustomConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CustomConfigElement)element).Name;
        }

        public CustomConfigElement this[int Index]
        {
            get
            {
                return (CustomConfigElement)BaseGet(Index);
            }

            set
            {
                if (BaseGet(Index) != null)
                {
                    BaseRemoveAt(Index);
                }

                BaseAdd(Index, value);
            }
        }

        new public CustomConfigElement this[string Name]
        {
            get
            {
                return (CustomConfigElement)BaseGet(Name);
            }
        }

    }


    public class CustomConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }

            set
            {
                this["name"] = value;
            }
        }
    }
}
