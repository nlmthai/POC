﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Solar.Infrastructure.Repository.EntityFramework.DAL
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(string connection)
            : base(connection)
        {
            
        }

        static DatabaseContext()
        {
            Database.SetInitializer<DatabaseContext>(
                new DropCreateDatabaseIfModelChanges<DatabaseContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            CustomConfigSection configSection =
                (CustomConfigSection)ConfigurationManager.GetSection("CustomConfigSection");

            foreach (CustomConfigElement customAssembly in configSection.Assemblies)
            {
                Assembly assembly = Assembly.Load(customAssembly.Name);
                Type[] assemblies = assembly.GetExportedTypes();

                //if we add table before add config --> error
                //so we have to add config before add table
                //--> we have to have two loops to make sure that config is always added before table

                foreach (Type type in assemblies)
                {
                    //using table 's config
                    if (IsEntityTypeConfiguration(type))
                    {
                        dynamic instance = Activator.CreateInstance(type);
                        modelBuilder.Configurations.Add(instance);
                    }
                }

                foreach (Type type in assemblies)
                {
                    //create table
                    if (CanCreateTable(type))
                    {
                        MethodInfo method = typeof(DbModelBuilder).GetMethod("Entity");
                        method = method.MakeGenericMethod(new Type[] { type });
                        method.Invoke(modelBuilder, null);
                    }
                }
            }

            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// If class is interited from EntityTypeConfiguration T
        /// </summary>
        /// <param name="type">Type of class</param>
        /// <returns>True: interited and else</returns>
        private bool IsEntityTypeConfiguration(Type type)
        {
            return type.BaseType != null
                && !string.IsNullOrEmpty(type.BaseType.Namespace)
                && type.BaseType.Namespace == "System.Data.Entity.ModelConfiguration"
                && type.BaseType.Name.Contains("EntityTypeConfiguration");
        }

        /// <summary>
        /// Identify whether this class can create table or not
        /// </summary>
        /// <param name="type">Type of class</param>
        /// <returns>True: can and else</returns>
        private bool CanCreateTable(Type type)
        {
            return type.IsClass
                && type.BaseType != null
                && type.BaseType.BaseType != null
                && type.BaseType.BaseType.FullName == "Solar.Core.DomainEntity";
        }
    }
}
