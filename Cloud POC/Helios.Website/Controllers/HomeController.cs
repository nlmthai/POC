﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Helios.Service.Contract;
using Newtonsoft.Json;

namespace Helios.Website.Controllers
{
    public class HomeController : Controller
    {
        private readonly string baseServiceUri =
            ConfigurationManager.AppSettings["ServiceUri"];

        private readonly string productServiceUriSegment = "/api/products/";


        public async Task<ActionResult> Index()
        {
            string productSku = "Sku1";
            int productId = 1;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseServiceUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP POST
                var product = new ProductDto() { Sku = productSku, Name = "Product Name" };
                HttpResponseMessage response = await client.PostAsJsonAsync(productServiceUriSegment, product);
                if (response.IsSuccessStatusCode)
                {
                    // HTTP PUT
                    product.Name = "New Product Name";
                    response = await client.PutAsJsonAsync(productServiceUriSegment + productId.ToString(), product);

                    // HTTP DELETE
                    response = await client.DeleteAsync(productServiceUriSegment);
                }

                // HTTP GET
                response = await client.GetAsync(productServiceUriSegment + productSku.ToString());
                if (response.IsSuccessStatusCode)
                {
                    var productList = await response.Content.ReadAsAsync<List<ProductDto>>();
                }
            }

            return View();
        }
    }
}
