﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using Solar.Web.Utility;

namespace Helios.Website
{
    public class CookieManager : CookieManagerBase
    {
        public CookieManager() : base()
        {
        }

        private static readonly CookieManager instance = new CookieManager();

        public static CookieManager Instance
        {
            get { return instance; }
        }

        [Expire("GuestCookieTimeoutInDays")]
        public int? GuestId
        {
            get
            {
                int guestId;

                var cookie = GetCookieValue("GuestId");

                if (cookie == null || !Int32.TryParse(cookie.Value, out guestId))
                {
                    return null;
                }

                return guestId;
            }
            set
            {
                SetCookieValue("GuestId", value.ToString());
            }
        }        
    }
}