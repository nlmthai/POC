﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Solar.Core
{
    public class DomainEntityTracker
    {
        public static IList<ChangeItem> FindChanges(object previousObj, object currentObj)
        {
            List<ChangeItem> changes = new List<ChangeItem>();
            var properties = previousObj != null ? previousObj.GetType().GetProperties() : null;
            if (properties == null) 
                properties = currentObj != null ? currentObj.GetType().GetProperties() : null;
            if (properties == null) 
                return changes;

            foreach (var prop in properties)
            {
                var trackChangeAtts = prop.GetCustomAttributes(typeof(TrackChangeAttribute), true);
                if (trackChangeAtts == null || trackChangeAtts.Count() == 0)
                    continue;
                TrackChangeAttribute trackChangeAtt = (TrackChangeAttribute)trackChangeAtts[0];

                object oldValue = prop.GetValue(previousObj, null);
                object newValue = prop.GetValue(currentObj, null);
                if (prop.PropertyType.IsEnum || prop.PropertyType.IsPrimitive || prop.PropertyType == typeof(string))
                {
                    if (( oldValue == null && newValue != null) 
                        || (oldValue != null && newValue == null)
                        || (oldValue != null && newValue != null && !oldValue.Equals(newValue))
                        )
                    {
                        changes.Add(new ChangeItem
                        {
                            FieldName = String.Format("{0}.{1}", currentObj.GetType().Name, prop.Name),
                            NiceName = trackChangeAtt.NiceName,
                            NewValue = newValue != null ? newValue.ToString() : "Empty",
                            OldValue = oldValue != null ? oldValue.ToString() : "Empty",
                            ModifiedDate = DateTime.Now
                        });
                    }
                }
                else
                {
                    changes.AddRange(FindChanges(oldValue, newValue));
                }
            }

            return changes;

        }
    }

    public class ChangeItem
    {
        public string NiceName { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class TrackChangeAttribute : Attribute
    {
        public string NiceName { get; set; }
   }
}
