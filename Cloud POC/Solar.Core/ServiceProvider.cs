/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.IO;
using System.Reflection;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;

namespace Solar.Core
{
    public class ServiceProvider:IDisposable
    {
        public readonly string DefaultWindsorConfigurationXmlFile;

        #region Singleton Representation

        static ServiceProvider()
        {
            Instance = new ServiceProvider();
        }

        public static ServiceProvider Instance { get; private set; }

        private ServiceProvider()
        {
            string assemblyUriLocation = Assembly.GetExecutingAssembly().CodeBase;
            string assemblyLocation = new Uri(assemblyUriLocation).LocalPath;
            DefaultWindsorConfigurationXmlFile = Path.Combine(
                Path.GetDirectoryName(assemblyLocation),
                "Solar.Core.Windsor.xml");

            this.container = new WindsorContainer(new XmlInterpreter(DefaultWindsorConfigurationXmlFile));
        }

        #endregion

        private readonly IWindsorContainer container;

        public TService GetService<TService>(string serviceId)
            where TService : class
        {
            return this.container.Resolve<TService>(serviceId);
        }

        public void Dispose()
        {
            container.Dispose();
        }
    }
}