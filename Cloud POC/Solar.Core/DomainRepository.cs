/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using Solar.Infrastructure.Repository;

namespace Solar.Core
{
    public class DomainRepository : IDisposable
    {
        protected DomainRepository(IRepository repository)
        {
            this.repository = repository;
        }

        private readonly IRepository repository;
        
        public static DomainRepository Open()
        {
            return Open(RepositoryFactory.DefaultRepositoryFactory);
        }

        public static DomainRepository Open(RepositoryFactory repositoryFactory)
        {
            return Open(OpenMode.UseSharedRepositoryIfAny, repositoryFactory);
        }

        public static DomainRepository Open(OpenMode openMode, RepositoryFactory repositoryFactory)
        {
            switch (openMode)
            {
                case OpenMode.AlwaysCreateSeparateRepository:
                    return CreateRepository(repositoryFactory.ToString());

                case OpenMode.UseSharedRepositoryIfAny:
                default:
                    return RepositoryScope.ActiveRepository ?? CreateRepository(repositoryFactory.ToString());
            }
        }

        private static DomainRepository CreateRepository(string repositoryFactoryName)
        {
            var repositoryFactory = ServiceProvider.Instance.GetService<IRepositoryFactory>(repositoryFactoryName);

            Contract.Assert(repositoryFactory != null);

            return new DomainRepository(repositoryFactory.CreateRepository());
        }

        public void Close()
        {
            this.Dispose();
        }

        #region IDisposable Members

        private bool disposed = false;

        public void Dispose()
        {
            if (RepositoryScope.IsOutOfScope(this))
            {
                if (!this.disposed)
                {
                    this.repository.Dispose();

                    this.disposed = true;
                }
            }
        }

        #endregion

        public TEntity Get<TEntity>(object id)
            where TEntity : DomainEntity
        {
            return this.repository.Get<TEntity>(id);
        }

        public IList<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter)
            where TEntity : DomainEntity
        {
            return this.repository.Get(filter);
        }

        public IList<TEntity> ExecuteQuery<TEntity>(string tSQL) where TEntity : class
        {
            return this.repository.ExecuteQuery<TEntity>(tSQL);
        }

        public TEntity GetOne<TEntity>(Expression<Func<TEntity, bool>> filter)
            where TEntity : DomainEntity
        {
            return this.repository.Get(filter).FirstOrDefault();
        }

        public IQueryable<TEntity> Query<TEntity>()
            where TEntity : DomainEntity
        {
            return this.repository.Query<TEntity>();
        }

        public object Add<TEntity>(TEntity entity)
            where TEntity : DomainEntity
        {
            RequiresAllRulesConformed(entity);

            return this.repository.Add(entity);
        }

        public void Update<TEntity>(TEntity entity)
            where TEntity : DomainEntity
        {
            RequiresAllRulesConformed(entity);

            this.repository.Update(entity);
        }

        public void AddOrUpdate<TEntity>(TEntity entity)
            where TEntity : DomainEntity
        {
            RequiresAllRulesConformed(entity);

            this.repository.AddOrUpdate(entity);
        }

        public void Remove<TEntity>(TEntity entity)
            where TEntity : DomainEntity
        {
            this.repository.Remove(entity);
        }

        public void Refresh<TEntity>(TEntity entity)
            where TEntity : DomainEntity
        {
            this.repository.Refresh(entity);
        }

        public void Stale<TEntity>(TEntity entity)
            where TEntity : DomainEntity
        {
            this.repository.Stale(entity);
        }

        public IUnitOfWork CreateUnitOfWork()
        {
            return this.repository.CreateUnitOfWork();
        }

        private static void RequiresAllRulesConformed<TEntity>(TEntity entity)
            where TEntity : DomainEntity
        {
            var brokenRules = entity.GetBrokenRules();

            if (brokenRules.Count > 0)
            {
                throw new RuleBrokenException(brokenRules);
            }
        }

        public enum OpenMode
        {
            UseSharedRepositoryIfAny,
            AlwaysCreateSeparateRepository
        }
    }

    public class RuleBrokenException : ApplicationException
    {
        public RuleBrokenException(IEnumerable<string> brokenRules)
        {
            this.BrokenRules = brokenRules;
        }

        public IEnumerable<string> BrokenRules { get; private set; }
    }

    public enum RepositoryFactory
    {
        DefaultRepositoryFactory,
        MssqlRepositoryFactory,
        PostgreSqlRepositoryFactory
    }
}