﻿using System;

namespace Solar.Core
{
    [AttributeUsage(AttributeTargets.Property|AttributeTargets.Field)]
    public class MaxLengthAttribute : Attribute
    {
        public int Length { get; private set; }
        public MaxLengthAttribute(int length)
        {
            Length = length;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class ReadOnlyAttribute : Attribute
    { }
}
