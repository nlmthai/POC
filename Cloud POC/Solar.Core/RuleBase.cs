﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System.Security;
using Solar.Infrastructure.Repository;

[assembly: SecurityRules(SecurityRuleSet.Level1)]
namespace Solar.Core
{
    public abstract class RuleBase<TEntity> : IRule<TEntity>
    {
        public abstract string Description { get; }

        public abstract bool Validate(TEntity entity);

        public string Name
        {
            get { return this.GetType().Name; }
        }
    }
}
