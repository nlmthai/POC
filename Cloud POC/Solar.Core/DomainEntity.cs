﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Solar.Infrastructure.Repository;

namespace Solar.Core
{
    public abstract class DomainEntity
    {
        public virtual int Id { get; set; }
        public virtual DateTime? ModifiedDate { get; set; }
        
        public override int GetHashCode()
        {
            var idHashCode = (this.Id).ToString().GetHashCode();

            return String.Format("{0}:{1}", this.GetEntityType().AssemblyQualifiedName, idHashCode).GetHashCode();
        }

        public override bool Equals(object entity)
        {
            return entity != null
                && entity.GetHashCode() == this.GetHashCode();
        }

        protected Type GetEntityType()
        {
            Type type = this.GetType();

            // proxy?
            while (!type.Assembly.Equals(typeof(DomainEntity).Assembly))
            {
                type = type.BaseType;
            }

            return type;
        }
    }

    public static class EntityExtensions_RuleManager
    {
        /// <summary>
        /// Validates domain entity against predefined rules.
        /// </summary>
        /// <typeparam name="TEntity">Type of domain entity</typeparam>
        /// <param name="entity">Domain entity to validate</param>
        /// <returns>List of broken rules</returns>
        public static IList<string> GetBrokenRules<TEntity>(this TEntity entity)
            where TEntity : DomainEntity
        {
            return RuleManager<TEntity>.Default.Validate(entity).ToList();
        }


        /// <summary>
        /// Validates domain entity against predefined rules.
        /// </summary>
        /// <typeparam name="TEntity">Type of domain entity</typeparam>
        /// <param name="entity">Domain entity to validate</param>
        /// <returns>True if valid, false otherwise</returns>
        //public static bool Validate<TEntity>(this TEntity entity)
        //    where TEntity : DomainEntity
        //{
        //    return RuleManager<TEntity>.Default.Validate(entity).Any();
        //}
    }
}
