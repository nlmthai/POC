﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

namespace Solar.Core
{
    public abstract class DomainEntity<TKey> : DomainEntity
        where TKey : struct
    {
        /// <summary>
        /// Id of domain entity.
        /// </summary>
        public new virtual int Id 
        {
            get
            {
                return base.Id;
            }
            protected set
            {
                base.Id = value;
            }
        }

        /// <summary>
        /// Returns true if domain entity is transient (not exist in repository just yet).
        /// Returns false otherwise.
        /// </summary>
        //public override bool IsTransient
        //{
        //    get
        //    {
        //        return base.Id == null || base.Id.Equals(default(TKey));
        //    }
        //}

        public virtual long Version { get; protected set; }
    }

    public interface IReadOnly
    {
        
    }
}
