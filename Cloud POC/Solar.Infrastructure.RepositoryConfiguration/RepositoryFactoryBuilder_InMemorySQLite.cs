/*
 * Copyright by TMC - TMA Microsoft Center - 2013
 */

using System;
using System.Data;
using Solar.Infrastructure.Repository.NHibernate;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Tool.hbm2ddl;
using Solar.Core;
using System.Reflection;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Conventions.Instances;
using Helios.Domain;
using Solar.Infrastructure.Repository;

namespace Solar.Infrastructure.RepositoryConfiguration
{
    public class RepositoryFactoryBuilder_InMemorySQLite : RepositoryFactoryBuilderBase
    {
        public static RepositoryFactoryBuilder_InMemorySQLite Instance { get; private set; }

        static RepositoryFactoryBuilder_InMemorySQLite()
        {
            Instance = new RepositoryFactoryBuilder_InMemorySQLite();
        }

        private InMemorySQLiteRepositoryFactory repositoryFactory;

        protected InMemorySQLiteRepositoryFactory RepositoryFactory
        {
            get
            {
                if (this.configuration == null)
                {
                    this.Configure();

                    this.ExportSchema();
                }

                if (this.repositoryFactory == null)
                {
                    this.repositoryFactory = new InMemorySQLiteRepositoryFactory(configuration.BuildSessionFactory());
                }

                return this.repositoryFactory;
            }
        }

        protected override Configuration ConfigureCore()
        {
            return ConfigureCore("Data Source=:memory:;Version=3;New=True;Pooling=True;Max Pool Size=1;");
        }

        /// <summary>
        /// Configures throw-away storage for automation testing
        /// </summary>
        /// <returns></returns>
        protected override Configuration ConfigureCore(string connectionString)
        {
            this.dialect = new SQLiteDialect();

            var databaseConfiguration = SQLiteConfiguration.Standard
                .ConnectionString(connectionString)                
                .UseReflectionOptimizer();

            return Fluently.Configure()
                .Database(databaseConfiguration)
                .Mappings(m =>
                {
                    m.AutoMappings.Add(BuildAutoMappingModel());
                })
                .BuildConfiguration();
        }

        private AutoPersistenceModel BuildAutoMappingModel()
        {
            return AutoMap.AssemblyOf<Account>()
                .Where(t => t.IsSubclassOf(typeof(DomainEntity)))
                .Conventions.Add<SchemaPrefixConvention>()
                .Conventions.Add(PrimaryKey.Name.Is(i => "Id"))
                .Conventions.Add(ForeignKey.Format((m, t) => (m != null ? m.Name : t.Name) + "Id"))
                .Conventions.Add(ConventionBuilder.HasMany.Always(i =>
                {
                    i.Cascade.SaveUpdate();
                    i.Inverse();
                }));
        }

        private class SchemaPrefixConvention : IClassConvention
        {
            private string ExtractFromNameSpace(string entityNamespace)
            {
                return "dbo";
            }

            public void Apply(IClassInstance instance)
            {
                instance.Schema(ExtractFromNameSpace(instance.EntityType.Namespace));
            }
        }

        protected override void ExportSchemaCore(Action<string> processLog)
        {
            new SchemaExport(this.ConfigureCore()).Execute(processLog ?? ((string s) => { }), true, false,
                this.RepositoryFactory.SingleConnection,
                null);
        }

        public override IRepositoryFactory BuildRepositoryFactory(
            string frameworkType, string connectionProfileName)
        {
            return this.RepositoryFactory;
        }
    }

    public class InMemorySQLiteRepositoryFactory : IRepositoryFactory
    {
        public IDbConnection SingleConnection { get; private set; }
        private readonly ISessionFactory sessionFactory;

        public InMemorySQLiteRepositoryFactory(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;

            using (var session = this.sessionFactory.OpenSession())
            {
                this.SingleConnection = session.Connection;
            }

            this.SingleConnection.Open();
        }

        public ISession OpenSession()
        {
            return this.sessionFactory.OpenSession(this.SingleConnection);
        }

        #region IRepositoryFactory Members

        public IRepository CreateRepository()
        {
            return new Solar.Infrastructure.Repository.NHibernate.Repository(this.OpenSession());
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            this.SingleConnection.Close();
            this.sessionFactory.Dispose();
        }

        #endregion
    }
}