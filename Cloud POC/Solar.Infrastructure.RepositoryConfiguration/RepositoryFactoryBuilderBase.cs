﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Diagnostics.Contracts;
using System.IO;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Tool.hbm2ddl;
using Solar.Infrastructure.Repository;
using NH = Solar.Infrastructure.Repository.NHibernate;
using EF = Solar.Infrastructure.Repository.EntityFramework;
using Solar.Infrastructure.Repository.EntityFramework.DAL;

namespace Solar.Infrastructure.RepositoryConfiguration
{
    public abstract class RepositoryFactoryBuilderBase
    {
        protected Configuration configuration;
        protected Dialect dialect;

        /// <summary>
        /// Configures throw-away storage for automation testing
        /// </summary>
        /// <returns></returns>
        protected void Configure()
        {
            this.configuration = this.ConfigureCore();

            this.QuoteIdentifiers();
        }

        private void QuoteIdentifiers()
        {
            Contract.Assert(this.dialect != null);

            foreach (var mapping in this.configuration.ClassMappings)
            {
                //if (!mapping.Table.IsQuoted)
                //{
                //    mapping.Table.Name = this.dialect.QuoteForTableName(mapping.Table.Name);
                //}

                //if (!mapping.Table.IsSchemaQuoted)
                //{
                //    mapping.Table.Schema = this.dialect.QuoteForSchemaName(mapping.Table.Schema ?? "dbo");
                //}

                foreach (var columnMapping in mapping.Table.ColumnIterator)
                {
                    if (!columnMapping.IsQuoted)
                    {
                        columnMapping.Name = this.dialect.QuoteForColumnName(columnMapping.Name);
                    }
                }
            }
        }

        protected abstract Configuration ConfigureCore();
        protected abstract Configuration ConfigureCore(string connectionString);

        protected virtual void ExportSchemaCore(Action<string> processLog)
        {
            new SchemaExport(this.configuration).Execute(processLog ?? ((string s) => { }), true, false);
        }

        public void ExportSchema()
        {
            this.ExportSchemaCore(null);
        }

        public void ExportSchema(string logFileName)
        {
            Contract.Requires(configuration != null);

            using (var file = File.CreateText(logFileName))
            {
                this.ExportSchemaCore(file.WriteLine);
            }
        }

        protected virtual void UpdateSchemaCore(Action<string> processLog)
        {
            new SchemaUpdate(this.configuration).Execute(processLog ?? ((string s) => { }), true);
        }

        public void UpdateSchema()
        {
            this.UpdateSchemaCore(null);
        }

        public void UpdateSchema(string logFileName)
        {
            Contract.Requires(configuration != null);

            using (var file = File.CreateText(logFileName))
            {
                this.UpdateSchemaCore(file.WriteLine);
            }
        }

        public virtual IRepositoryFactory BuildRepositoryFactory(
            string frameworkType,
            string connectionProfileName = "DefaultConnection")
        {
            string connString = System.Configuration.ConfigurationManager
                            .ConnectionStrings[connectionProfileName].ConnectionString;

            switch (frameworkType)
            {
                case "NHibernate":
                    if (this.configuration == null)
                    {
                        if (string.IsNullOrEmpty(connString))
                        {
                            this.configuration = this.ConfigureCore();
                        }
                        else
                        {
                            this.configuration = this.ConfigureCore(connString);
                        }

                        this.QuoteIdentifiers();
                    }

                    if (Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings["ExportSchema"]))
                    {
                        this.ExportSchema();
                    }
                    else if (Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings["UpdateSchema"]))
                    {
                        this.UpdateSchema();
                    }

                    return new NH.RepositoryFactory(configuration.BuildSessionFactory());

                case "EntityFramework":
                    return new EF.RepositoryFactory(connString);
                default:
                    return null;
            }
        }
    }
}