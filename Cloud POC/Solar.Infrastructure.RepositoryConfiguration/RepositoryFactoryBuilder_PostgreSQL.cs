﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */
using System.Configuration;
using Helios.Domain;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Conventions.Instances;
using NHibernate.Dialect;
using Solar.Core;
using Configuration = NHibernate.Cfg.Configuration;

namespace Solar.Infrastructure.RepositoryConfiguration
{
    public class RepositoryFactoryBuilder_PostgreSQL : RepositoryFactoryBuilderBase
    {
        /// <summary>
        /// Configures throw-away storage for automation testing
        /// </summary>
        /// <returns></returns>
        protected override Configuration ConfigureCore()
        {
            return ConfigureCore(ConfigurationManager.ConnectionStrings["PostgreSQLConnection"].ConnectionString);
        }

        protected override Configuration ConfigureCore(string connectionString)
        {
            this.dialect = new PostgreSQLDialect();

            var databaseConfiguration = PostgreSQLConfiguration.PostgreSQL82
                .ConnectionString(connectionString)
                .AdoNetBatchSize(10)
                .UseReflectionOptimizer();

            return Fluently.Configure()
                .Database(databaseConfiguration)
                .Mappings(m => 
                 {
                     m.AutoMappings.Add(BuildAutoMappingModel());
                })
                .BuildConfiguration();
        }

        private AutoPersistenceModel BuildAutoMappingModel()
        {
            return AutoMap.AssemblyOf<Account>()
                .Where(t => t.IsSubclassOf(typeof(DomainEntity)))
                .Conventions.Add<SchemaPrefixConvention>()
                .Conventions.Add<StringColumnLengthConvention>()
                .Conventions.Add<ReadOnlyConvention>()
                .Conventions.Add(PrimaryKey.Name.Is(i => "Id"), ForeignKey.Format((m, t) => (m != null ? m.Name : t.Name) + "Id"))
                .Conventions.Add(ConventionBuilder.Id.Always(id =>
                {
                    id.CustomSqlType("Serial");
                    id.GeneratedBy.Increment();
                }))
                .Conventions.Add(ConventionBuilder.Reference.Always(i =>
                {
                    i.Cascade.SaveUpdate();
                }))
                .Conventions.Add(ConventionBuilder.HasMany.Always(i =>
                {
                    
                    i.Cascade.AllDeleteOrphan();
                    i.BatchSize(10);
                }))
                ;
        }

        #region Conventions
        private class SchemaPrefixConvention : IClassConvention
        {
            private string ExtractFromNameSpace(string entityNamespace)
            {
                return "public";
                //return (entityNamespace.Substring(entityNamespace.LastIndexOf('.') + 1));
            }

            public void Apply(IClassInstance instance)
            {
                instance.Schema(ExtractFromNameSpace(instance.EntityType.Namespace));
            }
        }

        private class StringColumnLengthConvention : AttributePropertyConvention<MaxLengthAttribute>
        {
            protected override void Apply(MaxLengthAttribute attribute, IPropertyInstance instance)
            {
                instance.Length(attribute.Length); 
            }
        }

        private class ReadOnlyConvention : AttributePropertyConvention<ReadOnlyAttribute>
        {
            protected override void Apply(ReadOnlyAttribute attribute, IPropertyInstance instance)
            {
                instance.ReadOnly();
            }
        }
        #endregion
    }
}