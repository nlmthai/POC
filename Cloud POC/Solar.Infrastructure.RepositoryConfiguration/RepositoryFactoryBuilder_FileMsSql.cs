using System;
using System.IO;
using System.Reflection;
using NHibernate.Cfg;

namespace Solar.Infrastructure.RepositoryConfiguration
{
    public class RepositoryFactoryBuilder_FileMsSql : RepositoryFactoryBuilderBase
    {
        /// <summary>
        /// Configures throw-away storage for automation testing
        /// </summary>
        /// <returns></returns>
        protected override Configuration ConfigureCore()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Configures location for file-based SQL Server database
        /// </summary>
        private static void ConfigureDataDirectory()
        {
            string assemblyCodeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uriBuilder = new UriBuilder(assemblyCodeBase);
            string assemblyDirectory = Path.GetDirectoryName(uriBuilder.Path);
            string databaseDirectory = Path.Combine(assemblyDirectory, "Database");

            AppDomain.CurrentDomain.SetData("DataDirectory", databaseDirectory);
        }
    }
}