﻿using Solar.Core;
using FluentNHibernate.Mapping;

namespace Solar.Infrastructure.RepositoryConfiguration.Mappings
{
    public class ImmutableDomainEntityMap<TDomainEntity> : ClassMap<TDomainEntity>
        where TDomainEntity : DomainEntity<int>
    {
        protected ImmutableDomainEntityMap()
        {
            ReadOnly();
            Id(e => e.Id).GeneratedBy.Identity().UnsavedValue(0);
        }
    }
}
