﻿using Solar.Core;

namespace Solar.Infrastructure.RepositoryConfiguration.Mappings
{
    public class DomainEntityMap<TDomainEntity> : ImmutableDomainEntityMap<TDomainEntity>
        where TDomainEntity : DomainEntity<int>
    {
        public DomainEntityMap()
        {
            Not.ReadOnly();
            OptimisticLock.Version();
            Version(e => e.Version).Column("Version");
        }
    }
}
