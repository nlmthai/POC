﻿using Solar.Core;
using Helios.Domain;

namespace Solar.Infrastructure.RepositoryConfiguration.Mappings
{
    public class ProductMap : ImmutableDomainEntityMap<Product>
    {
        public ProductMap()
        {
            //Schema("dbo");
            Table("Product");

            Map(p => p.Sku);
            Map(p => p.Name);
        }
    }
}
