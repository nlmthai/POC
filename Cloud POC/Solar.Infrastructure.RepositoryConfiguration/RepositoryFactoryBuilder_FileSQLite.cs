﻿using Bamboo.Core;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Solar.Infrastructure.RepositoryConfiguration.Mappings;
using NHibernate.Cfg;
using NHibernate.Dialect;

namespace Solar.Infrastructure.RepositoryConfiguration
{
    public class RepositoryFactoryBuilder_FileSQLite : RepositoryFactoryBuilderBase
    {
        public static RepositoryFactoryBuilder_FileSQLite Instance { get; private set; }

        static RepositoryFactoryBuilder_FileSQLite()
        {
            Instance = new RepositoryFactoryBuilder_FileSQLite();
        }

        protected IRepositoryFactory repositoryFactory;

        /// <summary>
        /// Configures throw-away storage for automation testing
        /// </summary>
        /// <returns></returns>
        protected override Configuration ConfigureCore()
        {
            this.dialect = new SQLiteDialect();

            var databaseConfiguration = SQLiteConfiguration.Standard
                .ConnectionString("Data Source=FutureVision.db;Version=3;")
                .Cache(s => s.UseQueryCache())
                .UseReflectionOptimizer();

            return Fluently.Configure()
                .Database(databaseConfiguration)
                .Mappings(cfg => cfg.FluentMappings.AddFromAssemblyOf<ProductMap>())
                .BuildConfiguration();
        }
    }
}