﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Solar.Web.Mvc
{
    public static class UrlHelperExtensions
    {
        public static MvcHtmlString Action<TController>(this UrlHelper urlHelper, Expression<Func<TController, ActionResult>> expression)
            where TController : Controller
        {
            return new MvcHtmlString(urlHelper.RouteUrl(ControllerExtensions.CreateRoutes(expression)));
        }

        public static MvcHtmlString ActionEx<TController>(this UrlHelper urlHelper, Expression<Func<TController, ActionResult>> expression, bool requiredSsl)
            where TController : Controller
        {
            var requestUrl = HttpContext.Current.Request;

            return new MvcHtmlString(urlHelper.RouteUrl(ControllerExtensions.CreateRoutes(expression)));
        }

        public static MvcHtmlString ActionEx<TController>(this UrlHelper urlHelper, Expression<Func<TController, ActionResult>> expression)
            where TController : Controller
        {
            return ActionEx(urlHelper, expression, false);
        }
    }
}
