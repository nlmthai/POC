﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Solar.Web.Mvc
{
    public static partial class HtmlHelperExtensions
    {
        public static CheckBoxBuilder<TModel> CheckBoxEx<TModel>(this HtmlHelper htmlHelper, Expression<Func<TModel, object>> expression)
        {
            return new CheckBoxBuilder<TModel>(htmlHelper, expression);
        }
    }

    public class CheckBoxBuilder<TModel> : HtmlInputElementBuilder<TModel>
    {
        internal CheckBoxBuilder(HtmlHelper htmlHelper, Expression<Func<TModel, object>> expression)
            : base(htmlHelper, expression)
        {
            this.value = this.expression.Compile()((TModel)htmlHelper.ViewData.Model);
        }

        protected override MvcHtmlString BuildCore()
        {
            bool @checked = false;
            
            Boolean.TryParse(this.value.ToString(), out @checked);

            return this.htmlHelper.CheckBox(htmlHelper.BuildBindingPath(this.expression), @checked, this.htmlAttributes);
        }                
    }
}
