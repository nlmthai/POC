﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System.Web.Mvc;

namespace Solar.Web.Mvc
{
    public abstract class HtmlElementBuilder
    {        
        protected HtmlHelper htmlHelper;        
        protected object htmlAttributes;

        public HtmlElementBuilder(HtmlHelper htmlHelper)
        {
            this.htmlHelper = htmlHelper;
        }

        public HtmlHelper HtmlHelper { get { return this.htmlHelper; } }                

        internal HtmlElementBuilder HtmlAttributesCore(object htmlAttributes)
        {
            this.htmlAttributes = htmlAttributes;

            return this;
        }

        public abstract MvcHtmlString Build();

        public MvcHtmlString ToHtmlString()
        {
            return this.Build();
        }

        //public override string ToString()
        //{
        //    return this.Build().ToHtmlString();
        //}        
    }

    public static class HtmlElementBuilderExtensions
    {
        public static TBuilder HtmlAttributes<TBuilder>(this TBuilder builder, object htmlAttributes)
            where TBuilder : HtmlElementBuilder
        {
            return (TBuilder) builder.HtmlAttributesCore(htmlAttributes);
        }
    }    
}
