﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace Solar.Web.Utility
{
    public class CacheManager
    {
        public CacheManager(string cacheDirectory = "Cache")
        {
            this.CachePath = HttpContext.Current.Server.MapPath(string.Format("/{0}", cacheDirectory));
        }

        public string CachePath { get; set; }

        public void Store(string key, object data, Repository repository = Repository.Disk)
        {
            if (repository == Repository.Memory)
            {
                StoreToMemory(key, data, Int32.Parse(ConfigurationManager.AppSettings["CacheExpirationInMinute"]));
            }
            else
            {
                Store(key, data);
            }
        }

        public T Get<T>(string key, Repository repository = Repository.Disk)
        {
            return repository == Repository.Memory
                       ? GetFromMemory<T>(key)
                       : Get<T>(key);
        }

        #region Store to memory

        /// <summary>
        /// Add item into memory cache
        /// </summary>
        private void StoreToMemory(string key, object data, int expirationInMinute = 5)
        {
            if (HttpContext.Current.Cache[key] == null)
            {
                HttpContext.Current.Cache.Add(Hash(key), data, null,
                                              DateTime.Now.AddMinutes(expirationInMinute),
                                              Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
            }
        }

        /// <summary>
        /// Get cached item
        /// </summary>
        private T GetFromMemory<T>(string key)
        {
            var cached = HttpContext.Current.Cache[Hash(key)];

            if (cached != null && cached is T)
            {
                return (T)cached;
            }

            return default(T);
        }

        public void Destroy(string key)
        {
            HttpContext.Current.Cache.Remove(Hash(key));
        }

        public void DestroyAll()
        {
            var cache = HttpContext.Current.Cache;

            foreach (DictionaryEntry entry in cache)
            {
                HttpContext.Current.Cache.Remove((string)entry.Key);
            }
        }

        #endregion

        #region Store to File

        /// <summary>
        /// Store item into file
        /// </summary>
        private void Store(string key, object data)
        {
            if (!string.IsNullOrEmpty(key))
            {
                if (!Exists(key) && data != null)
                {
                    Directory.CreateDirectory(CachePath);

                    using (var stream = File.Create(Path.Combine(CachePath, Hash(key))))
                    {
                        var formatter = new BinaryFormatter();

                        formatter.Serialize(stream, data);
                    }
                }
            }
        }

        /// <summary>
        /// Get cached item from file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        private T Get<T>(string key)
        {
            if (Exists(key))
            {
                using (var stream = File.OpenRead(Path.Combine(CachePath, Hash(key))))
                {
                    var formatter = new BinaryFormatter();

                    var obj = formatter.Deserialize(stream);

                    if (obj != null && obj is T)
                    {
                        return (T)obj;
                    }
                }
            }

            return default(T);
        }

        public bool DeleteAll()
        {
            try
            {
                Directory.Delete(CachePath, true);

                return true;
            }
            catch
            {
                bool hasErrors = false;

                try
                {
                    var files = Directory.GetFiles(CachePath);

                    foreach (var file in files)
                    {
                        File.Delete(file);
                    }
                }
                catch
                {
                    hasErrors = true;
                }

                return !hasErrors;
            }
        }

        private bool Exists(string key)
        {
            if (!string.IsNullOrEmpty(key))
            {
                var pathToCache = string.Format("{0}\\{1}", CachePath, Hash(key));

                return File.Exists(pathToCache);
            }

            return false;
        }

        private string Hash(string input)
        {
            try
            {
                byte[] originalBytes = Encoding.UTF8.GetBytes(input.ToLower());

                MD5 encryptType = new MD5CryptoServiceProvider();

                byte[] encodedBytes = encryptType.ComputeHash(originalBytes);

                return BitConverter.ToString(encodedBytes).Replace("-", "").ToLower();
            }
            catch
            {
                return string.Empty;
            }
        }

        #endregion
    }

    public enum Repository
    {
        Memory,
        Disk
    }
}
