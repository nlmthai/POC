﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Configuration;
using System.Web;

namespace Solar.Web.Utility
{
    public class CookieManagerBase
    {
        public CookieManagerBase() { }        

        public static HttpCookie GetCookieValue(string key)
        {
            if (HttpContext.Current.Request.Cookies[key] == null)
            {
                return null;
            }

            return HttpContext.Current.Request.Cookies[key];
        }

        public static void SetCookieValue(string key, string value, DateTime? expiresWhen)
        {
            var cookie = new HttpCookie(key, value);

            if (expiresWhen.HasValue)
            {
                cookie.Expires = expiresWhen.Value;
            };

            HttpContext.Current.Response.Cookies.Set(cookie);
        }

        public static void SetCookieValue(string key, string value, TimeSpan timeToLive)
        {
            SetCookieValue(key, value, DateTime.Now.Add(timeToLive));
        }

        public static void SetCookieValue(string key, string value)
        {
            SetCookieValue(key, value, DateTime.Now.AddDays(Double.Parse(ConfigurationManager.AppSettings["GuestCookieTimeoutInDays"])));
        }

        [AttributeUsage(AttributeTargets.Property)]
        public class ExpireAttribute : Attribute
        {
            public ExpireAttribute(TimeSpan timeout)
            {
                this.Timeout = timeout;
            }

            public ExpireAttribute(string appSettingKey)
            {
                var reader = new System.Configuration.AppSettingsReader();

                object value = reader.GetValue(appSettingKey, typeof(int));

                if (value == null)
                {
                    throw new ArgumentException("AppSetting {0} could not be found.", appSettingKey);
                }

                var timeoutInDays = (int)value;

                this.Timeout = TimeSpan.FromDays(timeoutInDays);
            }

            public TimeSpan Timeout { get; private set; }
        }
    }
}
