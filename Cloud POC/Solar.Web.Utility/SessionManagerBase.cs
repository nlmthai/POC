﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System.Web;

namespace Solar.Web.Utility
{
    public class SessionManagerBase
    {
        public SessionManagerBase() { }        

        public void Clear()
        {
            HttpContext.Current.Session.RemoveAll();
        }

        public void Abandon()
        {
            HttpContext.Current.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", null));

            HttpContext.Current.Session.Abandon();
        }
    }
}
