/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Solar.Utility
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            Contract.Requires(enumerable != null);
            Contract.Requires(action != null);

            foreach (var item in enumerable)
            {
                action(item);
            }

            return enumerable;
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> enumerable, Func<T, bool> action)
        {
            Contract.Requires(enumerable != null);
            Contract.Requires(action != null);

            foreach (var item in enumerable)
            {
                if (!action(item))
                {
                    break;
                }
            }

            return enumerable;
        }

        public static IEnumerable<IEnumerable<T>> GroupsOf<T>(this IEnumerable<T> enumerable, int groupSize)
        {
            IList<T> group = new List<T>();

            foreach (T item in enumerable)
            {
                if (group.Count < groupSize)
                {
                    group.Add(item);
                }

                if (group.Count == groupSize)
                {
                    yield return group;

                    group = new List<T>();
                }
            }

            if (group.Count > 0)
            {
                yield return group;
            }
        }
    }
}
