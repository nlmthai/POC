﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Reflection;

namespace Solar.Utility
{
    public class EnumDescriptionAttribute : Attribute
    {
        private string description;

        public EnumDescriptionAttribute(string description)
        {
            this.description = description;
        }

        public string Description
        {
            get { return this.description; }
        }
    }

    public static class EnumDescriptionExtensions
    {        
        public static string GetEnumDescription(this Enum @enum)
        {
            FieldInfo fieldInfo = @enum.GetType().GetField(@enum.ToString());

            EnumDescriptionAttribute[] attributes = (EnumDescriptionAttribute[]) fieldInfo.GetCustomAttributes(typeof(EnumDescriptionAttribute), false);

            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }

            return String.Empty;
        }
    }
}
