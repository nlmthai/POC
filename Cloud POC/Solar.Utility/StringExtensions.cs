﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System.Globalization;

namespace Solar.Utility
{
    public static class StringExtensions
    {
        public static string Titlize(this string text)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(text);            
        }

        public static bool InsensitiveEquals(this string strA, string strB)
        {
            return strA.ToLower().Equals(strB.ToLower());
        }
    }
}
