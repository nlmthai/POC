﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bamboo.Helpers
{
    public class ExceptionExtension : Exception
    {
        public ExceptionExtension(string message)
            : base(message)
        {
        }

        public static ExceptionExtension InnerException(string objectName, string service, string method, string nameSpace, ExceptionType exceptionType)
        {
            return new ExceptionExtension(string.Format("{0} is {1} from {2} service under {3} method in {4}", objectName, exceptionType.ToString(), service, method, nameSpace));
        }
    }

    public enum ExceptionType
    {
        NullReferenceException
    }
}
