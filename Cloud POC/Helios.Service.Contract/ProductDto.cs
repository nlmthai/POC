﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helios.Service.Contract
{
    public class ProductDto : DtoBase
    {
        public ProductDto()
        {            
            this.ModifiedDate = DateTime.Now;
        }

        public string Sku { get; set; }
        public string Name { get; set; }
        public string Test { get; set; }
    }
}
