﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using log4net;
using log4net.Config;

namespace Solar.Infrastructure
{
    public class LogService
    {
        public static LogService Default { get; private set; }
        //public static LogService Administrative { get; private set; }

        static LogService()
        {
            Configure();

            Default = new LogService("Application.Default");
            //Administrative = new LogService("Application.Administrative");
        }

        private readonly ILog defaultLogger;
        private readonly ILog debugLogger;
        private readonly ILog infoLogger;
        private readonly ILog warnLogger;
        private readonly ILog errorLogger;
        private readonly ILog fatalLogger;

        public LogService(string name)
        {
            defaultLogger = LogManager.GetLogger(name);

            debugLogger = LogManager.Exists("Application.Debug") ?? defaultLogger;
            infoLogger = LogManager.Exists("Application.Info") ?? defaultLogger;
            warnLogger = LogManager.Exists("Application.Warn") ?? defaultLogger;
            errorLogger = LogManager.Exists("Application.Error") ?? defaultLogger;
            fatalLogger = LogManager.Exists("Application.Fatal") ?? defaultLogger;
        }

        public static void Configure()
        {
            XmlConfigurator.Configure();
        }

        public void Debug(string message, params object[] args)
        {
            if (debugLogger != null)
            {
                debugLogger.DebugFormat(message, args);
            }
        }

        public void Info(string message, params object[] args)
        {
            if (infoLogger != null)
            {
                infoLogger.InfoFormat(message, args);
            }
        }

        public void Warn(string message, params object[] args)
        {
            if (warnLogger != null)
            {
                warnLogger.WarnFormat(message, args);
            }
        }

        public void Error(string message, params object[] args)
        {
            if (errorLogger != null)
            {
                errorLogger.ErrorFormat(message, args);
            }
        }

        public void Fatal(string message, params object[] args)
        {
            if (fatalLogger != null)
            {
                fatalLogger.FatalFormat(message, args);
            }
        }
    }
}