﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using NCron;
using NCron.Fluent;
using Castle.Windsor;

namespace Solar.Infrastructure
{
    public static class WinsorIntegration
    {
        public static IWindsorContainer RootContainer { get; private set; }

        static WinsorIntegration()
        {
            RootContainer = new WindsorContainer();
            RootContainer.Install(new ConJobInstaller());
        }

        public static JobPart Run<TJob>(this SchedulePart part)
            where TJob : ICronJob
        {
            return part.With(() => RootContainer.CreateChildContainer()).Run(c => c.Resolve<TJob>());
        }

        public static JobPart Run(this SchedulePart part, string taskName)
        {
            return part.With(() => RootContainer.CreateChildContainer()).Run(c => c.Resolve<ICronJob>(taskName));
        }

        public static IWindsorContainer CreateChildContainer(this IWindsorContainer parent)
        {
            var child = new WindsorContainer();
            parent.AddChildContainer(child);

            return child;
        }
    }

    public class ConJobInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyNamed("Application.Service")
                                       .Where(type => type.IsSubclassOf(typeof(CronJob)))
                                       .WithService.DefaultInterfaces()
                                       .LifestyleSingleton()
                                       .Configure(c => c.Named(c.Implementation.Name)));
        }
    }
}
