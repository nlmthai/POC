﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using NCron;
using NCron.Service;
using NCron.Fluent;
using NCron.Fluent.Crontab;
using NCron.Fluent.Generics;


namespace Solar.Infrastructure
{
    public class NCronScheduler : IDisposable
    {
        private static readonly NCronScheduler Scheduler = new NCronScheduler();
        private readonly ISchedulingService _schedulingService;

        public ISchedulingService SchedulingService 
        {
            get { return _schedulingService; }
        }

        public static NCronScheduler Instance
        {
            get { return Scheduler; }
        }

        public NCronScheduler():this(new SchedulingService())
        { }

        public NCronScheduler(ISchedulingService schedulingService)
        {
            _schedulingService = schedulingService;
        }

        public void AddTask<T>(string cronExpression) where T : ICronJob
        {
            var taskName = typeof (T).Name;
            SchedulingService.At(cronExpression).Run(taskName);
        }

        public void Start()
        {
            SchedulingService.Start();
        }

        public void Stop()
        {
            SchedulingService.Stop();
        }

        public void Dispose()
        {
            Stop();
            if (SchedulingService is IDisposable)
            {
                (SchedulingService as IDisposable).Dispose();
            }
        }
    }
}
