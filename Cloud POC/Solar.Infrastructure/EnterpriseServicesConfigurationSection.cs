﻿using System.Configuration;

namespace Solar.Infrastructure
{
    public class EnterpriseServicesConfigurationSection : ConfigurationSection
    {
        public const string SectionElement = "enterpriseServices";
        public const string SmtpServerProperty = "smtpServer";

        [ConfigurationProperty(SmtpServerProperty)]
        public string SmtpServer 
        {
            get { return this[SmtpServerProperty] as string; }
            set { this[SmtpServerProperty] = value; }
        }

        public static EnterpriseServicesConfigurationSection GetConfigurationSection()
        {
            return ConfigurationManager.GetSection(SectionElement) as EnterpriseServicesConfigurationSection;
        }
    }
}