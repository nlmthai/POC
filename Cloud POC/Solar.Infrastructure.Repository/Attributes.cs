﻿using System;

namespace Solar.Infrastructure.Repository
{
    [AttributeUsage(AttributeTargets.Property|AttributeTargets.Field)]
    public class StringLengthAttribute : Attribute
    {
        public int Length { get; private set; }
        public StringLengthAttribute(int length)
        {
            Length = length;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class ReadOnlyAttribute : Attribute
    { }
}
