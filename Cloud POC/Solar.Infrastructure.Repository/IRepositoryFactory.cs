﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;

namespace Solar.Infrastructure.Repository
{
    public interface IRepositoryFactory : IDisposable
    {
        IRepository CreateRepository();
    }
}
