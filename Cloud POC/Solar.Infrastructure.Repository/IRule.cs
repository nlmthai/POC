﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

namespace Solar.Infrastructure.Repository
{
    public interface IRule<TEntity>
    {
        /// <summary>
        /// Rule name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Rule description (should be a constant string)
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Verifies the given entity against pre-defined rules (defined via RuleManager).
        /// </summary>
        /// <param name="entity">Entity to verify</param>
        /// <returns>true if all passes; false otherwise</returns>
        bool Validate(TEntity entity);

    }
}
