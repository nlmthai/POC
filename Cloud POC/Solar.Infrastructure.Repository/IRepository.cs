﻿/* 
 * Copyright (c) 2013 by TMC - TMA Microsoft Center
 * Floor 8, TMA Building - Quang Trung Software City
 * District 12, Ho Chi Minh City, Vietnam
 * Email: tmc@tma.com.vn
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Solar.Infrastructure.Repository
{
    public interface IRepository : IDisposable
    {
        TEntity Get<TEntity>(object id) where TEntity : class;
        IList<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class;
        IQueryable<TEntity> Query<TEntity>() where TEntity : class;
        //IQueryable<TEntity> Query<TEntity>(params string[] prefetchProperties);

        object Add(object entity);
        void Update(object entity);
        void Remove(object entity);
        void Refresh(object entity);
        void Stale(object entity);

        void AddOrUpdate(object entity);

        IList<TEntity> ExecuteQuery<TEntity>(string tSQL) where TEntity : class;

        IUnitOfWork CreateUnitOfWork();
    }
}
