﻿using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace Solar.Utility.Web
{
    public class CacheManager
    {
        //configuration
        private static string CachePath
        {
            get
            {
                return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["PathToCache"]);
            }
        }

        public static void Store(string key, object data, Repository repository)
        {
            if (repository == Repository.Memory)
            {
                StoreToMemory(key, data);
            }
            else
            {
                Store(key, data);
            }
        }

        public static T Get<T>(string key, Repository repository)
        {
            return repository == Repository.Memory
                       ? GetFromMemory<T>(key)
                       : Get<T>(key);
        }

        #region Store to memory

        /// <summary>
        /// Add item into memory cache
        /// </summary>
        private static void StoreToMemory(string key, object data)
        {
            if (HttpContext.Current.Cache[key] == null)
            {
                HttpContext.Current.Cache.Add(Hash(key), data, null,
                                              DateTime.Now.AddDays(Double.Parse("1")),
                                              Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
            }
        }

        /// <summary>
        /// Get cached item
        /// </summary>
        private static T GetFromMemory<T>(string key)
        {
            var cached = HttpContext.Current.Cache[Hash(key)];

            if (cached != null && cached is T)
            {
                return (T)cached;
            }

            return default(T);
        }

        public static void Destroy(string key)
        {
            HttpContext.Current.Cache.Remove(Hash(key));
        }

        public static void DestroyAll()
        {
            var cache = HttpContext.Current.Cache;

            foreach (DictionaryEntry entry in cache)
            {
                HttpContext.Current.Cache.Remove((string)entry.Key);
            }
        }

        #endregion

        #region Store to File

        /// <summary>
        /// Store item into file
        /// </summary>
        private static void Store(string key, object data)
        {
            if (!string.IsNullOrEmpty(key))
            {
                if (!Exists(key) && data != null)
                {
                    Directory.CreateDirectory(CachePath);

                    using (var stream = File.Create(Path.Combine(CachePath, Hash(key))))
                    {
                        var formatter = new BinaryFormatter();

                        formatter.Serialize(stream, data);
                    }
                }
            }
        }

        /// <summary>
        /// Get cached item from file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        private static T Get<T>(string key)
        {
            if (Exists(key))
            {
                using (var stream = File.OpenRead(Path.Combine(CachePath, Hash(key))))
                {
                    var formatter = new BinaryFormatter();

                    var obj = formatter.Deserialize(stream);

                    if (obj != null && obj is T)
                    {
                        return (T)obj;
                    }
                }
            }

            return default(T);
        }

        public static bool DeleteAll()
        {
            var cachePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["PathToCache"]);

            try
            {
                Directory.Delete(cachePath, true);

                return true;
            }
            catch (Exception ex)
            {
                bool hasErrors = false;

                try
                {
                    var files = Directory.GetFiles(cachePath);

                    foreach (var file in files)
                    {
                        File.Delete(file);
                    }
                }
                catch (Exception ex2)
                {
                    hasErrors = true;
                }

                return !hasErrors;
            }
        }

        private static bool Exists(string key)
        {
            if (!string.IsNullOrEmpty(key))
            {
                var pathToCache = string.Format("{0}\\{1}", CachePath, Hash(key));

                return File.Exists(pathToCache);
            }

            return false;
        }

        private static string Hash(string input)
        {
            try
            {
                byte[] originalBytes = Encoding.UTF8.GetBytes(input.ToLower());

                MD5 encryptType = new MD5CryptoServiceProvider();

                byte[] encodedBytes = encryptType.ComputeHash(originalBytes);

                return BitConverter.ToString(encodedBytes).Replace("-", "").ToLower();
            }
            catch
            {
                return string.Empty;
            }
        }

        #endregion
    }

    public enum CachingKey
    {
        GlobalCategories
    }

    public enum Repository
    {
        Memory,
        Disk
    }
}
