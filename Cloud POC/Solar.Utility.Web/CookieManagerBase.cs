﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Configuration;

namespace Solar.Utility.Web
{
    public class CookieManagerBase
    {
        public CookieManagerBase() { }        

        public static HttpCookie GetCookieValue(string key)
        {
            if (HttpContext.Current.Request.Cookies[key] == null)
            {
                return null;
            }

            return HttpContext.Current.Request.Cookies[key];
        }

        public static void SetCookieValue(string key, string value, DateTime? expiresWhen)
        {
            var cookie = new HttpCookie(key, value);

            if (expiresWhen.HasValue)
            {
                cookie.Expires = expiresWhen.Value;
            };

            HttpContext.Current.Response.Cookies.Set(cookie);
        }

        public static void SetCookieValue(string key, string value, TimeSpan timeToLive)
        {
            SetCookieValue(key, value, DateTime.Now.Add(timeToLive));
        }

        public static void SetCookieValue(string key, string value)
        {
            SetCookieValue(key, value, DateTime.Now.AddDays(Double.Parse(ConfigurationManager.AppSettings["CookieTimeoutInDays"])));
        }

        [AttributeUsage(AttributeTargets.Property)]
        public class ExpireAttribute : Attribute
        {
            public ExpireAttribute(TimeSpan timeout)
            {
                this.Timeout = timeout;
            }

            public ExpireAttribute(string appSettingKey)
            {
                var reader = new System.Configuration.AppSettingsReader();

                object value = reader.GetValue(appSettingKey, typeof(int));

                if (value == null)
                {
                    throw new ArgumentException("AppSetting {0} could not be found.", appSettingKey);
                }

                var timeoutInDays = (int)value;

                this.Timeout = TimeSpan.FromDays(timeoutInDays);
            }

            public TimeSpan Timeout { get; private set; }
        }
    }
}
