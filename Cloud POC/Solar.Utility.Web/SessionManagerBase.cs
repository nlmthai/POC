﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Solar.Utility.Web
{
    public class SessionManagerBase
    {
        public SessionManagerBase() { }        

        public void Clear()
        {
            HttpContext.Current.Session.RemoveAll();
        }

        public void Abandon()
        {
            HttpContext.Current.Response.Cookies.Add(new HttpCookie("Application_SessionId", null));

            HttpContext.Current.Session.Abandon();
        }
    }
}
