﻿using StackExchange.Redis;
using System;

namespace POC_CacheDemo.Services
{
    public interface IRedisCache
    {
        IDatabase GetDatabase();

        bool Exists(string key);

        void Save(string key, string value, TimeSpan? timeExpire = null);

        string Get(string key);

        void Remove(string key);

        void ClearAllDataBase();
    }

    public class RedisCacheService : IRedisCache
    {
        private static ConnectionMultiplexer RedisConnection;

        private IDatabase RedisCache;

        public RedisCacheService()
        {
            if (RedisConnection == null)
            {
                ConfigurationOptions config = new ConfigurationOptions()
                {
                    EndPoints = { "poc-contoso.redis.cache.windows.net" },
                    Ssl = true,
                    Password = "zbSlumsFiA1gmMaX/f1zaGrH7iw+5BrRUobs64cXOXE="
                };

                RedisConnection = ConnectionMultiplexer.Connect(config);

                // Using connect string
                //RedisConnection = ConnectionMultiplexer.Connect("poc-contoso.redis.cache.windows.net,ssl=true,password=zbSlumsFiA1gmMaX/f1zaGrH7iw+5BrRUobs64cXOXE=");
            }

            RedisCache = RedisConnection.GetDatabase();
        }

        public IDatabase GetDatabase()
        {
            return RedisConnection.GetDatabase();
        }

        public bool Exists(string key)
        {
            return RedisCache.KeyExists(key);
        }

        public void Save(string key, string value, TimeSpan? timeExpire = null)
        {
            timeExpire = timeExpire != null ? timeExpire : TimeSpan.FromMinutes(30);
            RedisCache.StringSet(key, value, timeExpire);
        }

        public string Get(string key)
        {
            return RedisCache.StringGet(key);
        }

        public void Remove(string key)
        {
            RedisCache.KeyDelete(key);
        }

        public void ClearAllDataBase()
        {
            var endpoints = RedisConnection.GetEndPoints(true);
            foreach (var item in endpoints)
            {
                var server = RedisConnection.GetServer(item);
                server.FlushAllDatabases();
            }
        }
    }
}