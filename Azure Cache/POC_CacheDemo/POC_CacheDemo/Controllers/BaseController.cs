﻿using POC_CacheDemo.Services;
using System.Web.Mvc;

namespace POC_CacheDemo.Controllers
{
    public class BaseController : Controller
    {
        protected RedisCacheService RedisCacheService;

        public BaseController()
        {
            RedisCacheService = new RedisCacheService();
        }
    }
}