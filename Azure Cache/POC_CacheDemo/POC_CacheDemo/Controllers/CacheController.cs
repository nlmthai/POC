﻿using POC_CacheDemo.Models;
using POC_CacheDemo.Services;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace POC_CacheDemo.Controllers
{
    public class CacheController : BaseController
    {
        private DataViewModel ListData;
        private List<BookViewModel> Cart = new List<BookViewModel>();

        public CacheController()
        {
            ListData = new DataViewModel()
            {
                ListBook = new List<BookViewModel>()
                {
                    new BookViewModel() { Id = 1, Name = "Book 1" },
                    new BookViewModel() { Id = 2, Name = "Book 2" },
                    new BookViewModel() { Id = 3, Name = "Book 3" },
                    new BookViewModel() { Id = 4, Name = "Book 4"}
                }
            };
        }

        [HttpPost]
        public ActionResult Index(string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
            {
                RedisCacheService.Save("userName", userName);
            }

            ViewBag.UserName = RedisCacheService.Get("userName");

            return View(ListData);
        }

        public ActionResult YourCart()
        {
            var cart = new List<BookViewModel>();
            if (Session["YourCart"] != null)
            {
                cart = StackExchangeRedisExtensions.Deserialize<List<BookViewModel>>((byte[])Session["YourCart"]);
            }

            return View(new DataViewModel() { ListBook = cart });
        }

        [HttpPost]
        public ActionResult AddOrRemoveToCart(int id, bool addToCart)
        {
            try
            {
                if (Session["YourCart"] != null)
                {
                    Cart = StackExchangeRedisExtensions.Deserialize<List<BookViewModel>>((byte[])Session["YourCart"]);
                }

                if (addToCart)
                {
                    var item = Cart.Where(x => x.Id == id).SingleOrDefault();
                    if (item == null)
                    {
                        var book = ListData.ListBook.Where(x => x.Id == id).SingleOrDefault();
                        if (book != null)
                        {
                            Cart.Add(book);
                        }
                    }
                }
                else
                {
                    var book = Cart.Where(x => x.Id == id).SingleOrDefault();
                    Cart.Remove(book);
                }

                Session.Add("YourCart", StackExchangeRedisExtensions.Serialize(Cart));

                return Json(new { result = true });
            }
            catch (Exception e)
            {
                return Json(new { result = false });
            }
        }
    }
}