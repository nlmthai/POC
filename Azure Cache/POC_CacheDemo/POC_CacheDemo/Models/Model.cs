﻿using System;
using System.Collections.Generic;

namespace POC_CacheDemo.Models
{
    [Serializable]
    public class BookViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class DataViewModel
    {
        public List<BookViewModel> ListBook { get; set; }
    }
}